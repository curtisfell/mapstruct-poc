package com.respec.billing.accounts.controllers;

import com.respec.billing.accounts.AccountsApplication;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = AccountsApplication.class)
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
@EnableAutoConfiguration
public class AccountsControllerTest
{
    @Autowired
    private MockMvc mockMvc;


    @Test
    public void get_gtv_account_by_id_and_fail_account_not_found_test() {

        String tenantId = "1"; // GTV account
        int accountId = 1234; // account does not exist

        try {

            final MvcResult mvcResult = this.mockMvc.perform(
                    get("/api/billing/account-services/{tenant}/get-account/{accountid}", tenantId, accountId))
                    .andExpect(status().is4xxClientError())
                    .andReturn();

            //assertThat(mvcResult.getResponse().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());


        } catch (Exception ex) {}

    }


    @Test
    public void get_gtv_account_by_id_success_test() {

        String tenantId = "1"; // GTV account
        int accountId = 12; // acct exists in GTV staging (any value between 10 and 149)

        try {

            final MvcResult mvcResult = this.mockMvc.perform(
                    get("/api/billing/account-services/{tenant}/get-account/{accountid}", tenantId, accountId))
                    .andExpect(status().is2xxSuccessful())
                    .andReturn();

            //assertThat(mvcResult.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());

        } catch (Exception ex) {}

    }


    @Test
    public void get_brm_account_by_id_success_test() {

        String tenantId = "3"; // BRM account (actually any value > 1)
        int accountId = 120; // can pass in any value...

        try {

            final MvcResult mvcResult = this.mockMvc.perform(
                    get("/api/billing/account-services/{tenant}/get-account/{accountid}", tenantId, accountId))
                    .andExpect(status().is2xxSuccessful())
                    .andReturn();

            //assertThat(mvcResult.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());

        } catch (Exception ex) {}

    }
}
