package com.respec.billing.accounts.services;

public class ServiceProcessingException extends RuntimeException  {

    public ServiceProcessingException(String message) {
        super(message);
    }

    public ServiceProcessingException(String message, Throwable err) {
        super(message, err);
    }
}
