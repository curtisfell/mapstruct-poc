package com.respec.billing.accounts.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.respec.billing.accounts.models.*;
import com.respec.billing.accounts.repositories.TenantConfigurationRepository;
import com.respec.billing.common.AccountConfiguration;
import com.respec.billing.common.Request;
import com.respec.billing.common.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@Service
public class AccountService
{

    private final GtvAccountService gtvAccountService;
    private final BrmAccountService brmAccountService;
    private final TenantConfigurationRepository tenantConfigurationRepository;

    @Autowired
    public AccountService(final GtvAccountService gtvAccountService, final BrmAccountService brmAccountService, TenantConfigurationRepository tenantConfigurationRepository) {

        this.gtvAccountService = gtvAccountService;
        this.brmAccountService = brmAccountService;
        this.tenantConfigurationRepository = tenantConfigurationRepository;

    }


    public Response getAccount(final HttpHeaders requestHeaders, final Request request) throws IOException, NoSuchAlgorithmException {

        AccountConfiguration conf =
                new AccountConfiguration(request.getTenantId());

        List<GenericAccount> accounts = new ArrayList<>();
        if(conf.getProvider().equals(AccountConfiguration.BillingProvider.GTV)) {
            GenericAccount ga = gtvAccountService.getAccount(request);
            accounts.add(ga);
        } else {
            GenericAccount ga = brmAccountService.getAccount(request);
            accounts.add(ga);
        }

        final Response response = new Response();
        response.setSuccessful(response!=null?true:false);
        response.setAccounts(accounts);
        return response;

    }


    public Response listAccounts(final HttpHeaders requestHeaders, final Request request) throws IOException, NoSuchAlgorithmException {

        //TODO were going to fix this
        AccountConfiguration conf =
                new AccountConfiguration(request.getTenantId());

        List<GenericAccount> accounts;
        if(conf.getProvider().equals(AccountConfiguration.BillingProvider.GTV)) {
            accounts = gtvAccountService.listAccounts(request);
        } else {
            accounts = brmAccountService.listAccounts(request);
        }

        //TODO map it to respec account obj

        final Response response = new Response();
        response.setSuccessful(response!=null?true:false);
        response.setAccounts(accounts);
        return response;

    }


    private String serializeRequest (final Request request) throws JsonProcessingException {

        final ObjectMapper requestMapper = new ObjectMapper();
        return requestMapper.writeValueAsString(request);

    }


    private Response deserializeResponse (final String responseString) throws JsonProcessingException, IOException {

        final ObjectMapper responseMapper = new ObjectMapper();
        return responseMapper.readValue(responseString, Response.class);

    }


}
