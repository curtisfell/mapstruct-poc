package com.respec.billing.accounts.models.gtv;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Address implements Serializable
{

    @SerializedName("address_type")
    @Expose
    private String addressType;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("purpose")
    @Expose
    private String purpose;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("line1")
    @Expose
    private String line1;
    @SerializedName("line2")
    @Expose
    private String line2;
    @SerializedName("region_or_state")
    @Expose
    private String regionOrState;
    @SerializedName("postal_code")
    @Expose
    private String postalCode;
    @SerializedName("email")
    @Expose
    private String email;
    private final static long serialVersionUID = -7534595489008916638L;

    /**
     * No args constructor for use in serialization
     *
     */
    public Address() {
    }

    /**
     *
     * @param country
     * @param purpose
     * @param city
     * @param addressType
     * @param regionOrState
     * @param postalCode
     * @param id
     * @param line2
     * @param line1
     * @param email
     */
    public Address(String addressType, String id, String purpose, String country, String city, String line1, String line2, String regionOrState, String postalCode, String email) {
        super();
        this.addressType = addressType;
        this.id = id;
        this.purpose = purpose;
        this.country = country;
        this.city = city;
        this.line1 = line1;
        this.line2 = line2;
        this.regionOrState = regionOrState;
        this.postalCode = postalCode;
        this.email = email;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getRegionOrState() {
        return regionOrState;
    }

    public void setRegionOrState(String regionOrState) {
        this.regionOrState = regionOrState;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("addressType", addressType).append("id", id).append("purpose", purpose).append("country", country).append("city", city).append("line1", line1).append("line2", line2).append("regionOrState", regionOrState).append("postalCode", postalCode).append("email", email).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(country).append(purpose).append(city).append(addressType).append(regionOrState).append(postalCode).append(id).append(line2).append(line1).append(email).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Address) == false) {
            return false;
        }
        Address rhs = ((Address) other);
        return new EqualsBuilder().append(country, rhs.country).append(purpose, rhs.purpose).append(city, rhs.city).append(addressType, rhs.addressType).append(regionOrState, rhs.regionOrState).append(postalCode, rhs.postalCode).append(id, rhs.id).append(line2, rhs.line2).append(line1, rhs.line1).append(email, rhs.email).isEquals();
    }

}