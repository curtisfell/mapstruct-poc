package com.respec.billing.accounts.models.gtv;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class BillCycle implements Serializable
{

    @SerializedName("bill_cycle_type")
    @Expose
    private String billCycleType;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("internal_render")
    @Expose
    private Boolean internalRender;
    @SerializedName("auto_bill")
    @Expose
    private Boolean autoBill;
    @SerializedName("include_external_charges")
    @Expose
    private Boolean includeExternalCharges;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;
    @SerializedName("current_bill_cycle_run")
    @Expose
    private CurrentBillCycleRun currentBillCycleRun;
    @SerializedName("day_of_month")
    @Expose
    private Integer dayOfMonth;
    private final static long serialVersionUID = 5281879139038938162L;

    /**
     * No args constructor for use in serialization
     *
     */
    public BillCycle() {
    }

    /**
     *
     * @param endDate
     * @param dayOfMonth
     * @param billCycleType
     * @param name
     * @param currentBillCycleRun
     * @param includeExternalCharges
     * @param id
     * @param currencyCode
     * @param startDate
     * @param internalRender
     * @param status
     * @param autoBill
     */
    public BillCycle(String billCycleType, String id, String status, String name, String startDate, String endDate, Boolean internalRender, Boolean autoBill, Boolean includeExternalCharges, String currencyCode, CurrentBillCycleRun currentBillCycleRun, Integer dayOfMonth) {
        super();
        this.billCycleType = billCycleType;
        this.id = id;
        this.status = status;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.internalRender = internalRender;
        this.autoBill = autoBill;
        this.includeExternalCharges = includeExternalCharges;
        this.currencyCode = currencyCode;
        this.currentBillCycleRun = currentBillCycleRun;
        this.dayOfMonth = dayOfMonth;
    }

    public String getBillCycleType() {
        return billCycleType;
    }

    public void setBillCycleType(String billCycleType) {
        this.billCycleType = billCycleType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Boolean getInternalRender() {
        return internalRender;
    }

    public void setInternalRender(Boolean internalRender) {
        this.internalRender = internalRender;
    }

    public Boolean getAutoBill() {
        return autoBill;
    }

    public void setAutoBill(Boolean autoBill) {
        this.autoBill = autoBill;
    }

    public Boolean getIncludeExternalCharges() {
        return includeExternalCharges;
    }

    public void setIncludeExternalCharges(Boolean includeExternalCharges) {
        this.includeExternalCharges = includeExternalCharges;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public CurrentBillCycleRun getCurrentBillCycleRun() {
        return currentBillCycleRun;
    }

    public void setCurrentBillCycleRun(CurrentBillCycleRun currentBillCycleRun) {
        this.currentBillCycleRun = currentBillCycleRun;
    }

    public Integer getDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(Integer dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("billCycleType", billCycleType).append("id", id).append("status", status).append("name", name).append("startDate", startDate).append("endDate", endDate).append("internalRender", internalRender).append("autoBill", autoBill).append("includeExternalCharges", includeExternalCharges).append("currencyCode", currencyCode).append("currentBillCycleRun", currentBillCycleRun).append("dayOfMonth", dayOfMonth).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(endDate).append(billCycleType).append(currentBillCycleRun).append(includeExternalCharges).append(internalRender).append(autoBill).append(dayOfMonth).append(name).append(id).append(currencyCode).append(startDate).append(status).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof BillCycle) == false) {
            return false;
        }
        BillCycle rhs = ((BillCycle) other);
        return new EqualsBuilder().append(endDate, rhs.endDate).append(billCycleType, rhs.billCycleType).append(currentBillCycleRun, rhs.currentBillCycleRun).append(includeExternalCharges, rhs.includeExternalCharges).append(internalRender, rhs.internalRender).append(autoBill, rhs.autoBill).append(dayOfMonth, rhs.dayOfMonth).append(name, rhs.name).append(id, rhs.id).append(currencyCode, rhs.currencyCode).append(startDate, rhs.startDate).append(status, rhs.status).isEquals();
    }

}