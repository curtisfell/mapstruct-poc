package com.respec.billing.accounts.models.gtv;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class BillingAccount implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("account_num")
    @Expose
    private String accountNum;
    @SerializedName("external_account_num")
    @Expose
    private String externalAccountNum;
    @SerializedName("bill_type")
    @Expose
    private String billType;
    @SerializedName("bill_cycle")
    @Expose
    private BillCycle billCycle;
    @SerializedName("responsible_party")
    @Expose
    private Customer responsibleParty;
    @SerializedName("recurring_payments")
    @Expose
    private List<RecurringPayment> recurringPayments = null;
    @SerializedName("billing_account_category")
    @Expose
    private BillingAccountCategory billingAccountCategory;
    @SerializedName("auto_payment_authorized")
    @Expose
    private Boolean autoPaymentAuthorized;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("status_reason")
    @Expose
    private StatusReason statusReason;
    @SerializedName("pending_charges_total")
    @Expose
    private Double pendingChargesTotal;
    @SerializedName("balance")
    @Expose
    private Double balance;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("custom_field_values")
    @Expose
    private List<Object> customFieldValues = null;
    @SerializedName("contacts")
    @Expose
    private List<Contact> contacts = null;
    @SerializedName("payment_term")
    @Expose
    private PaymentTerm paymentTerm;
    @SerializedName("tax_exempt")
    @Expose
    private Boolean taxExempt;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;
    @SerializedName("next_invoice_date")
    @Expose
    private String nextInvoiceDate;
    @SerializedName("responsible_account")
    @Expose
    private ResponsibleAccount responsibleAccount;
    @SerializedName("background_allocation")
    @Expose
    private Boolean backgroundAllocation;
    @SerializedName("preferred_language_code")
    @Expose
    private Object preferredLanguageCode;
    @SerializedName("minimum_invoice_amount")
    @Expose
    private Object minimumInvoiceAmount;
    @SerializedName("billing_schedules")
    @Expose
    private List<BillingSchedule> billingSchedules = null;
    @SerializedName("parent_billing_account")
    @Expose
    private BillingAccountRef parentBillingAccount = null;
    private final static long serialVersionUID = -5450767518846262749L;

    /**
     * No args constructor for use in serialization
     *
     */
    public BillingAccount() {
    }

    /**
     *
     * @param preferredLanguageCode
     * @param accountNum
     * @param endDate
     * @param responsibleParty
     * @param statusReason
     * @param balance
     * @param id
     * @param taxExempt
     * @param responsibleAccount
     * @param externalAccountNum
     * @param billType
     * @param billingAccountCategory
     * @param recurringPayments
     * @param backgroundAllocation
     * @param minimumInvoiceAmount
     * @param autoPaymentAuthorized
     * @param customFieldValues
     * @param billingSchedules
     * @param pendingChargesTotal
     * @param nextInvoiceDate
     * @param billCycle
     * @param paymentTerm
     * @param currencyCode
     * @param startDate
     * @param contacts
     * @param status
     */
    public BillingAccount(String id, String accountNum, String externalAccountNum, String billType, BillCycle billCycle, Customer responsibleParty, List<RecurringPayment> recurringPayments, BillingAccountCategory billingAccountCategory, Boolean autoPaymentAuthorized, String status, StatusReason statusReason, Double pendingChargesTotal, Double balance, String startDate, String endDate, List<Object> customFieldValues, List<Contact> contacts, PaymentTerm paymentTerm, Boolean taxExempt, String currencyCode, String nextInvoiceDate, ResponsibleAccount responsibleAccount, Boolean backgroundAllocation, Object preferredLanguageCode, Object minimumInvoiceAmount, List<BillingSchedule> billingSchedules, BillingAccountRef parentBillingAccount) {
        super();
        this.id = id;
        this.accountNum = accountNum;
        this.externalAccountNum = externalAccountNum;
        this.billType = billType;
        this.billCycle = billCycle;
        this.responsibleParty = responsibleParty;
        this.recurringPayments = recurringPayments;
        this.billingAccountCategory = billingAccountCategory;
        this.autoPaymentAuthorized = autoPaymentAuthorized;
        this.status = status;
        this.statusReason = statusReason;
        this.pendingChargesTotal = pendingChargesTotal;
        this.balance = balance;
        this.startDate = startDate;
        this.endDate = endDate;
        this.customFieldValues = customFieldValues;
        this.contacts = contacts;
        this.paymentTerm = paymentTerm;
        this.taxExempt = taxExempt;
        this.currencyCode = currencyCode;
        this.nextInvoiceDate = nextInvoiceDate;
        this.responsibleAccount = responsibleAccount;
        this.backgroundAllocation = backgroundAllocation;
        this.preferredLanguageCode = preferredLanguageCode;
        this.minimumInvoiceAmount = minimumInvoiceAmount;
        this.billingSchedules = billingSchedules;
        this.parentBillingAccount = parentBillingAccount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public String getExternalAccountNum() {
        return externalAccountNum;
    }

    public void setExternalAccountNum(String externalAccountNum) {
        this.externalAccountNum = externalAccountNum;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public BillCycle getBillCycle() {
        return billCycle;
    }

    public void setBillCycle(BillCycle billCycle) {
        this.billCycle = billCycle;
    }

    public Customer getResponsibleParty() {
        return responsibleParty;
    }

    public void setResponsibleParty(Customer responsibleParty) {
        this.responsibleParty = responsibleParty;
    }

    public List<RecurringPayment> getRecurringPayments() {
        return recurringPayments;
    }

    public void setRecurringPayments(List<RecurringPayment> recurringPayments) {
        this.recurringPayments = recurringPayments;
    }

    public BillingAccountCategory getBillingAccountCategory() {
        return billingAccountCategory;
    }

    public void setBillingAccountCategory(BillingAccountCategory billingAccountCategory) {
        this.billingAccountCategory = billingAccountCategory;
    }

    public Boolean getAutoPaymentAuthorized() {
        return autoPaymentAuthorized;
    }

    public void setAutoPaymentAuthorized(Boolean autoPaymentAuthorized) {
        this.autoPaymentAuthorized = autoPaymentAuthorized;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public StatusReason getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(StatusReason statusReason) {
        this.statusReason = statusReason;
    }

    public Double getPendingChargesTotal() {
        return pendingChargesTotal;
    }

    public void setPendingChargesTotal(Double pendingChargesTotal) {
        this.pendingChargesTotal = pendingChargesTotal;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<Object> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<Object> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public PaymentTerm getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(PaymentTerm paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public Boolean getTaxExempt() {
        return taxExempt;
    }

    public void setTaxExempt(Boolean taxExempt) {
        this.taxExempt = taxExempt;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getNextInvoiceDate() {
        return nextInvoiceDate;
    }

    public void setNextInvoiceDate(String nextInvoiceDate) {
        this.nextInvoiceDate = nextInvoiceDate;
    }

    public ResponsibleAccount getResponsibleAccount() {
        return responsibleAccount;
    }

    public void setResponsibleAccount(ResponsibleAccount responsibleAccount) {
        this.responsibleAccount = responsibleAccount;
    }

    public Boolean getBackgroundAllocation() {
        return backgroundAllocation;
    }

    public void setBackgroundAllocation(Boolean backgroundAllocation) {
        this.backgroundAllocation = backgroundAllocation;
    }

    public Object getPreferredLanguageCode() {
        return preferredLanguageCode;
    }

    public void setPreferredLanguageCode(Object preferredLanguageCode) {
        this.preferredLanguageCode = preferredLanguageCode;
    }

    public Object getMinimumInvoiceAmount() {
        return minimumInvoiceAmount;
    }

    public void setMinimumInvoiceAmount(Object minimumInvoiceAmount) {
        this.minimumInvoiceAmount = minimumInvoiceAmount;
    }

    public List<BillingSchedule> getBillingSchedules() {
        return billingSchedules;
    }

    public void setBillingSchedules(List<BillingSchedule> billingSchedules) {
        this.billingSchedules = billingSchedules;
    }

    public BillingAccountRef getParentBillingAccount() {
        return parentBillingAccount;
    }

    public void setParentBillingAccount(List<BillingAccountRef> billingSchedules) {
        this.parentBillingAccount = parentBillingAccount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("accountNum", accountNum).append("externalAccountNum", externalAccountNum).append("billType", billType).append("billCycle", billCycle).append("responsibleParty", responsibleParty).append("recurringPayments", recurringPayments).append("billingAccountCategory", billingAccountCategory).append("autoPaymentAuthorized", autoPaymentAuthorized).append("status", status).append("statusReason", statusReason).append("pendingChargesTotal", pendingChargesTotal).append("balance", balance).append("startDate", startDate).append("endDate", endDate).append("customFieldValues", customFieldValues).append("contacts", contacts).append("paymentTerm", paymentTerm).append("taxExempt", taxExempt).append("currencyCode", currencyCode).append("nextInvoiceDate", nextInvoiceDate).append("responsibleAccount", responsibleAccount).append("backgroundAllocation", backgroundAllocation).append("preferredLanguageCode", preferredLanguageCode).append("minimumInvoiceAmount", minimumInvoiceAmount).append("billingSchedules", billingSchedules).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(preferredLanguageCode).append(accountNum).append(endDate).append(responsibleParty).append(statusReason).append(balance).append(id).append(taxExempt).append(responsibleAccount).append(externalAccountNum).append(billType).append(billingAccountCategory).append(recurringPayments).append(backgroundAllocation).append(minimumInvoiceAmount).append(autoPaymentAuthorized).append(customFieldValues).append(billingSchedules).append(pendingChargesTotal).append(nextInvoiceDate).append(billCycle).append(paymentTerm).append(currencyCode).append(startDate).append(contacts).append(status).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof BillingAccount) == false) {
            return false;
        }
        BillingAccount rhs = ((BillingAccount) other);
        return new EqualsBuilder().append(preferredLanguageCode, rhs.preferredLanguageCode).append(accountNum, rhs.accountNum).append(endDate, rhs.endDate).append(responsibleParty, rhs.responsibleParty).append(statusReason, rhs.statusReason).append(balance, rhs.balance).append(id, rhs.id).append(taxExempt, rhs.taxExempt).append(responsibleAccount, rhs.responsibleAccount).append(externalAccountNum, rhs.externalAccountNum).append(billType, rhs.billType).append(billingAccountCategory, rhs.billingAccountCategory).append(recurringPayments, rhs.recurringPayments).append(backgroundAllocation, rhs.backgroundAllocation).append(minimumInvoiceAmount, rhs.minimumInvoiceAmount).append(autoPaymentAuthorized, rhs.autoPaymentAuthorized).append(customFieldValues, rhs.customFieldValues).append(billingSchedules, rhs.billingSchedules).append(pendingChargesTotal, rhs.pendingChargesTotal).append(nextInvoiceDate, rhs.nextInvoiceDate).append(billCycle, rhs.billCycle).append(paymentTerm, rhs.paymentTerm).append(currencyCode, rhs.currencyCode).append(startDate, rhs.startDate).append(contacts, rhs.contacts).append(status, rhs.status).isEquals();
    }

}