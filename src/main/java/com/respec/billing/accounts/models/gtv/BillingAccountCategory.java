package com.respec.billing.accounts.models.gtv;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class BillingAccountCategory implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("auto_pay_offset")
    @Expose
    private Integer autoPayOffset;
    @SerializedName("preferred_language_code")
    @Expose
    private Object preferredLanguageCode;
    @SerializedName("minimum_invoice_thresholds")
    @Expose
    private List<MinimumInvoiceThreshold> minimumInvoiceThresholds = null;
    @SerializedName("addresses")
    @Expose
    private List<Object> addresses = null;
    private final static long serialVersionUID = 7998409054688016889L;

    /**
     * No args constructor for use in serialization
     *
     */
    public BillingAccountCategory() {
    }

    /**
     *
     * @param preferredLanguageCode
     * @param addresses
     * @param name
     * @param description
     * @param id
     * @param minimumInvoiceThresholds
     * @param autoPayOffset
     * @param status
     */
    public BillingAccountCategory(String id, String description, String status, String name, Integer autoPayOffset, Object preferredLanguageCode, List<MinimumInvoiceThreshold> minimumInvoiceThresholds, List<Object> addresses) {
        super();
        this.id = id;
        this.description = description;
        this.status = status;
        this.name = name;
        this.autoPayOffset = autoPayOffset;
        this.preferredLanguageCode = preferredLanguageCode;
        this.minimumInvoiceThresholds = minimumInvoiceThresholds;
        this.addresses = addresses;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAutoPayOffset() {
        return autoPayOffset;
    }

    public void setAutoPayOffset(Integer autoPayOffset) {
        this.autoPayOffset = autoPayOffset;
    }

    public Object getPreferredLanguageCode() {
        return preferredLanguageCode;
    }

    public void setPreferredLanguageCode(Object preferredLanguageCode) {
        this.preferredLanguageCode = preferredLanguageCode;
    }

    public List<MinimumInvoiceThreshold> getMinimumInvoiceThresholds() {
        return minimumInvoiceThresholds;
    }

    public void setMinimumInvoiceThresholds(List<MinimumInvoiceThreshold> minimumInvoiceThresholds) {
        this.minimumInvoiceThresholds = minimumInvoiceThresholds;
    }

    public List<Object> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Object> addresses) {
        this.addresses = addresses;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("description", description).append("status", status).append("name", name).append("autoPayOffset", autoPayOffset).append("preferredLanguageCode", preferredLanguageCode).append("minimumInvoiceThresholds", minimumInvoiceThresholds).append("addresses", addresses).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(preferredLanguageCode).append(addresses).append(name).append(description).append(id).append(minimumInvoiceThresholds).append(autoPayOffset).append(status).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof BillingAccountCategory) == false) {
            return false;
        }
        BillingAccountCategory rhs = ((BillingAccountCategory) other);
        return new EqualsBuilder().append(preferredLanguageCode, rhs.preferredLanguageCode).append(addresses, rhs.addresses).append(name, rhs.name).append(description, rhs.description).append(id, rhs.id).append(minimumInvoiceThresholds, rhs.minimumInvoiceThresholds).append(autoPayOffset, rhs.autoPayOffset).append(status, rhs.status).isEquals();
    }

}