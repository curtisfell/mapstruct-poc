package com.respec.billing.accounts.models;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class GenericAccount implements Serializable {

	private String accountNumber;
	private String externalAccountNumber;
	private double accountBalance;
	private String parentAccountId;
	private String currency;
	private GenericAddress billingAddress;
	private GenericAddress serviceAddress;
	private List<GenericContact> contacts;
	private String Bdom;
	private String type;
	private List<GenericPaymentMethod> paymentMethods;
	private GenericCustomer genericResponsibleParty;

	public GenericAccount() {
	}

	public GenericAccount(String accountNumber, String externalAccountNumber, double accountBalance, String parentAccountId, String currency, GenericAddress billingAddress, GenericAddress serviceAddress, List<GenericContact> contacts, String bdom, String type, List<GenericPaymentMethod> paymentMethods, GenericCustomer genericResponsibleParty) {
		this.accountNumber = accountNumber;
		this.externalAccountNumber = externalAccountNumber;
		this.accountBalance = accountBalance;
		this.parentAccountId = parentAccountId;
		this.currency = currency;
		this.billingAddress = billingAddress;
		this.serviceAddress = serviceAddress;
		this.contacts = contacts;
		Bdom = bdom;
		this.type = type;
		this.paymentMethods = paymentMethods;
		this.genericResponsibleParty = genericResponsibleParty;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getExternalAccountNumber() {
		return externalAccountNumber;
	}

	public void setExternalAccountNumber(String externalAccountNumber) {
		this.externalAccountNumber = externalAccountNumber;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public String getParentAccountId() {
		return parentAccountId;
	}

	public void setParentAccountId(String parentAccountId) {
		this.parentAccountId = parentAccountId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public GenericAddress getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(GenericAddress billingAddress) {
		this.billingAddress = billingAddress;
	}

	public GenericAddress getServiceAddress() {
		return serviceAddress;
	}

	public void setServiceAddress(GenericAddress serviceAddress) {
		this.serviceAddress = serviceAddress;
	}

	public List<GenericContact> getContacts() {
		return contacts;
	}

	public void setContacts(List<GenericContact> contacts) {
		this.contacts = contacts;
	}

	public String getBdom() {
		return Bdom;
	}

	public void setBdom(String bdom) {
		Bdom = bdom;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<GenericPaymentMethod> getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(List<GenericPaymentMethod> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public GenericCustomer getGenericResponsibleParty() {
		return genericResponsibleParty;
	}

	public void setGenericResponsibleParty(GenericCustomer genericResponsibleParty) {
		this.genericResponsibleParty = genericResponsibleParty;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof GenericAccount)) return false;
		GenericAccount that = (GenericAccount) o;
		return Double.compare(that.getAccountBalance(), getAccountBalance()) == 0 && Objects.equals(getAccountNumber(), that.getAccountNumber()) && Objects.equals(getExternalAccountNumber(), that.getExternalAccountNumber()) && Objects.equals(getParentAccountId(), that.getParentAccountId()) && Objects.equals(getCurrency(), that.getCurrency()) && Objects.equals(getBillingAddress(), that.getBillingAddress()) && Objects.equals(getServiceAddress(), that.getServiceAddress()) && Objects.equals(getContacts(), that.getContacts()) && Objects.equals(getBdom(), that.getBdom()) && Objects.equals(getType(), that.getType()) && Objects.equals(getPaymentMethods(), that.getPaymentMethods()) && Objects.equals(getGenericResponsibleParty(), that.getGenericResponsibleParty());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getAccountNumber(), getExternalAccountNumber(), getAccountBalance(), getParentAccountId(), getCurrency(), getBillingAddress(), getServiceAddress(), getContacts(), getBdom(), getType(), getPaymentMethods(), getGenericResponsibleParty());
	}

	@Override
	public String toString() {
		return "GenericAccount{" +
				"accountNumber='" + accountNumber + '\'' +
				", externalAccountNumber='" + externalAccountNumber + '\'' +
				", accountBalance=" + accountBalance +
				", parentAccountId='" + parentAccountId + '\'' +
				", currency='" + currency + '\'' +
				", billingAddress=" + billingAddress +
				", serviceAddress=" + serviceAddress +
				", contacts=" + contacts +
				", Bdom='" + Bdom + '\'' +
				", type='" + type + '\'' +
				", paymentMethods=" + paymentMethods +
				", genericResponsibleParty=" + genericResponsibleParty +
				'}';
	}
}
