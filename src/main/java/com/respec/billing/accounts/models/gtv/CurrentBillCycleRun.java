package com.respec.billing.accounts.models.gtv;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class CurrentBillCycleRun implements Serializable
{

    @SerializedName("bill_run_type")
    @Expose
    private String billRunType;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = -1577673342830037784L;

    /**
     * No args constructor for use in serialization
     *
     */
    public CurrentBillCycleRun() {
    }

    /**
     *
     * @param billRunType
     * @param id
     */
    public CurrentBillCycleRun(String billRunType, String id) {
        super();
        this.billRunType = billRunType;
        this.id = id;
    }

    public String getBillRunType() {
        return billRunType;
    }

    public void setBillRunType(String billRunType) {
        this.billRunType = billRunType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("billRunType", billRunType).append("id", id).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(billRunType).append(id).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CurrentBillCycleRun) == false) {
            return false;
        }
        CurrentBillCycleRun rhs = ((CurrentBillCycleRun) other);
        return new EqualsBuilder().append(billRunType, rhs.billRunType).append(id, rhs.id).isEquals();
    }

}