package com.respec.billing.accounts.models.gtv;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class CustomField implements Serializable
{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("custom_field_type")
    @Expose
    private String customFieldType;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = 3058105153133176511L;

    /**
     * No args constructor for use in serialization
     *
     */
    public CustomField() {
    }

    /**
     *
     * @param name
     * @param id
     * @param customFieldType
     */
    public CustomField(String name, String customFieldType, String id) {
        super();
        this.name = name;
        this.customFieldType = customFieldType;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustomFieldType() {
        return customFieldType;
    }

    public void setCustomFieldType(String customFieldType) {
        this.customFieldType = customFieldType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("customFieldType", customFieldType).append("id", id).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(name).append(id).append(customFieldType).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CustomField) == false) {
            return false;
        }
        CustomField rhs = ((CustomField) other);
        return new EqualsBuilder().append(name, rhs.name).append(id, rhs.id).append(customFieldType, rhs.customFieldType).isEquals();
    }

}