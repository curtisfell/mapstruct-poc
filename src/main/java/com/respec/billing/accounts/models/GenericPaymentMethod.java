package com.respec.billing.accounts.models;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class GenericPaymentMethod implements Serializable
{
    private String paymentMethodType;
    private String multiFactorAuth;
    private String id;

    public GenericPaymentMethod() {
    }

    public GenericPaymentMethod(String paymentMethodType, String multiFactorAuth, String id) {
        this.paymentMethodType = paymentMethodType;
        this.multiFactorAuth = multiFactorAuth;
        this.id = id;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public String getMultiFactorAuth() {
        return multiFactorAuth;
    }

    public void setMultiFactorAuth(String multiFactorAuth) {
        this.multiFactorAuth = multiFactorAuth;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("paymentMethodType", paymentMethodType).append("multiFactorAuth", multiFactorAuth).append("id", id).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(paymentMethodType).append(multiFactorAuth).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof GenericPaymentMethod) == false) {
            return false;
        }
        GenericPaymentMethod rhs = ((GenericPaymentMethod) other);
        return new EqualsBuilder().append(id, rhs.id).append(paymentMethodType, rhs.paymentMethodType).append(multiFactorAuth, rhs.multiFactorAuth).isEquals();
    }

}