package com.respec.billing.accounts.models;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class GenericContact implements Serializable {
    private String partyType;
    private String firstName;
    private String lastName;
    private String middleName;
    private String suffix;
    private String dob;
    private String company;
    private String department;
    private String title;
    private String taxIdNumber;
    private List<GenericAddress> addresses = null;
    private GenericContactCategory contactCategory;
    private String id;
    private final static long serialVersionUID = -5328432171393434551L;

    public GenericContact() {
    }

    public GenericContact(String partyType, String firstName, String lastName, String middleName, String suffix, String dob, String company, String department, String title, String taxIdNumber, List<GenericAddress> addresses, GenericContactCategory contactCategory, String id) {
        this.partyType = partyType;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.suffix = suffix;
        this.dob = dob;
        this.company = company;
        this.department = department;
        this.title = title;
        this.taxIdNumber = taxIdNumber;
        this.addresses = addresses;
        this.contactCategory = contactCategory;
        this.id = id;
    }

    public String getPartyType() {
        return partyType;
    }

    public void setPartyType(String partyType) {
        this.partyType = partyType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTaxIdNumber() {
        return taxIdNumber;
    }

    public void setTaxIdNumber(String taxIdNumber) {
        this.taxIdNumber = taxIdNumber;
    }

    public List<GenericAddress> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<GenericAddress> addresses) {
        this.addresses = addresses;
    }

    public GenericContactCategory getContactCategory() {
        return contactCategory;
    }

    public void setContactCategory(GenericContactCategory contactCategory) {
        this.contactCategory = contactCategory;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GenericContact)) return false;
        GenericContact that = (GenericContact) o;
        return Objects.equals(getPartyType(), that.getPartyType()) && Objects.equals(getFirstName(), that.getFirstName()) && Objects.equals(getLastName(), that.getLastName()) && Objects.equals(getMiddleName(), that.getMiddleName()) && Objects.equals(getSuffix(), that.getSuffix()) && Objects.equals(getDob(), that.getDob()) && Objects.equals(getCompany(), that.getCompany()) && Objects.equals(getDepartment(), that.getDepartment()) && Objects.equals(getTitle(), that.getTitle()) && Objects.equals(getTaxIdNumber(), that.getTaxIdNumber()) && Objects.equals(getAddresses(), that.getAddresses()) && Objects.equals(getContactCategory(), that.getContactCategory()) && Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPartyType(), getFirstName(), getLastName(), getMiddleName(), getSuffix(), getDob(), getCompany(), getDepartment(), getTitle(), getTaxIdNumber(), getAddresses(), getContactCategory(), getId());
    }

    @Override
    public String toString() {
        return "GenericContact{" +
                "partyType='" + partyType + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", suffix='" + suffix + '\'' +
                ", dob='" + dob + '\'' +
                ", company='" + company + '\'' +
                ", department='" + department + '\'' +
                ", title='" + title + '\'' +
                ", taxIdNumber='" + taxIdNumber + '\'' +
                ", addresses=" + addresses +
                ", contactCategory=" + contactCategory +
                ", id='" + id + '\'' +
                '}';
    }
}