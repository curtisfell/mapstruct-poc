package com.respec.billing.accounts.models.gtv;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class StatusReason implements Serializable
{

    @SerializedName("reason_type")
    @Expose
    private String reasonType;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = -4394433434861499766L;

    /**
     * No args constructor for use in serialization
     *
     */
    public StatusReason() {
    }

    /**
     *
     * @param name
     * @param description
     * @param id
     * @param reasonType
     * @param status
     */
    public StatusReason(String reasonType, String name, String description, String status, String id) {
        super();
        this.reasonType = reasonType;
        this.name = name;
        this.description = description;
        this.status = status;
        this.id = id;
    }

    public String getReasonType() {
        return reasonType;
    }

    public void setReasonType(String reasonType) {
        this.reasonType = reasonType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("reasonType", reasonType).append("name", name).append("description", description).append("status", status).append("id", id).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(name).append(description).append(id).append(reasonType).append(status).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof StatusReason) == false) {
            return false;
        }
        StatusReason rhs = ((StatusReason) other);
        return new EqualsBuilder().append(name, rhs.name).append(description, rhs.description).append(id, rhs.id).append(reasonType, rhs.reasonType).append(status, rhs.status).isEquals();
    }

}