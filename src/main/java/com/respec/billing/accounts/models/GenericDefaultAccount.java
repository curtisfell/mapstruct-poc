package com.respec.billing.accounts.models;

import java.io.Serializable;
import java.util.List;

public class GenericDefaultAccount implements Serializable {

    private String accountNum;
    private String externalAccountNum;
    private List<GenericDiscount> genericDiscounts = null;
    private String id;

    public GenericDefaultAccount() {
    }

    public GenericDefaultAccount(String accountNum, String externalAccountNum, List<GenericDiscount> genericDiscounts, String id) {
        this.accountNum = accountNum;
        this.externalAccountNum = externalAccountNum;
        this.genericDiscounts = genericDiscounts;
        this.id = id;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public String getExternalAccountNum() {
        return externalAccountNum;
    }

    public void setExternalAccountNum(String externalAccountNum) {
        this.externalAccountNum = externalAccountNum;
    }

    public List<GenericDiscount> getGenericDiscounts() {
        return genericDiscounts;
    }

    public void setGenericDiscounts(List<GenericDiscount> genericDiscounts) {
        this.genericDiscounts = genericDiscounts;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
