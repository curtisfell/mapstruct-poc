package com.respec.billing.accounts.models.gtv;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class PaymentMethod implements Serializable
{

    @SerializedName("payment_method_type")
    @Expose
    private String paymentMethodType;
    @SerializedName("custom_field_values")
    @Expose
    private List<CustomFieldValue> customFieldValues = null;
    @SerializedName("multi_factor_auth")
    @Expose
    private String multiFactorAuth;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = 3240803442572040299L;

    /**
     * No args constructor for use in serialization
     *
     */
    public PaymentMethod() {
    }

    /**
     *
     * @param customFieldValues
     * @param multiFactorAuth
     * @param id
     * @param paymentMethodType
     */
    public PaymentMethod(String paymentMethodType, List<CustomFieldValue> customFieldValues, String multiFactorAuth, String id) {
        super();
        this.paymentMethodType = paymentMethodType;
        this.customFieldValues = customFieldValues;
        this.multiFactorAuth = multiFactorAuth;
        this.id = id;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public List<CustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(List<CustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public String getMultiFactorAuth() {
        return multiFactorAuth;
    }

    public void setMultiFactorAuth(String multiFactorAuth) {
        this.multiFactorAuth = multiFactorAuth;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("paymentMethodType", paymentMethodType).append("customFieldValues", customFieldValues).append("multiFactorAuth", multiFactorAuth).append("id", id).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(customFieldValues).append(paymentMethodType).append(multiFactorAuth).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PaymentMethod) == false) {
            return false;
        }
        PaymentMethod rhs = ((PaymentMethod) other);
        return new EqualsBuilder().append(id, rhs.id).append(customFieldValues, rhs.customFieldValues).append(paymentMethodType, rhs.paymentMethodType).append(multiFactorAuth, rhs.multiFactorAuth).isEquals();
    }

}