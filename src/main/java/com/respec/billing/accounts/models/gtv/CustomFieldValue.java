package com.respec.billing.accounts.models.gtv;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class CustomFieldValue implements Serializable
{

    @SerializedName("custom_field")
    @Expose
    private CustomField customField;
    @SerializedName("custom_field_value_type")
    @Expose
    private String customFieldValueType;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = 483266339720860023L;

    /**
     * No args constructor for use in serialization
     *
     */
    public CustomFieldValue() {
    }

    /**
     *
     * @param customFieldValueType
     * @param id
     * @param value
     * @param customField
     */
    public CustomFieldValue(CustomField customField, String customFieldValueType, String value, String id) {
        super();
        this.customField = customField;
        this.customFieldValueType = customFieldValueType;
        this.value = value;
        this.id = id;
    }

    public CustomField getCustomField() {
        return customField;
    }

    public void setCustomField(CustomField customField) {
        this.customField = customField;
    }

    public String getCustomFieldValueType() {
        return customFieldValueType;
    }

    public void setCustomFieldValueType(String customFieldValueType) {
        this.customFieldValueType = customFieldValueType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("customField", customField).append("customFieldValueType", customFieldValueType).append("value", value).append("id", id).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(value).append(customField).append(customFieldValueType).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CustomFieldValue) == false) {
            return false;
        }
        CustomFieldValue rhs = ((CustomFieldValue) other);
        return new EqualsBuilder().append(id, rhs.id).append(value, rhs.value).append(customField, rhs.customField).append(customFieldValueType, rhs.customFieldValueType).isEquals();
    }

}