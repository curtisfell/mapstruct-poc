package com.respec.billing.accounts.models;

import java.io.Serializable;

public class GenericBillCycle implements Serializable {

        private String billCycleType;
        private String name;
        private String id;

    public GenericBillCycle() {
    }

    public GenericBillCycle(String billCycleType, String name, String id) {
        this.billCycleType = billCycleType;
        this.name = name;
        this.id = id;
    }

        public String getBillCycleType() {
        return billCycleType;
    }

        public void setBillCycleType(String billCycleType) {
        this.billCycleType = billCycleType;
    }

        public String getName() {
        return name;
    }

        public void setName(String name) {
        this.name = name;
    }

        public String getId() {
        return id;
    }

        public void setId(String id) {
        this.id = id;
    }

}
