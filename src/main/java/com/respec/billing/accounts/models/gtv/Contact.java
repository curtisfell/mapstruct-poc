package com.respec.billing.accounts.models.gtv;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Contact implements Serializable
{

    @SerializedName("party_type")
    @Expose
    private String partyType;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("middle_name")
    @Expose
    private String middleName;
    @SerializedName("suffix")
    @Expose
    private String suffix;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("department")
    @Expose
    private String department;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("tax_id_number")
    @Expose
    private String taxIdNumber;
    @SerializedName("addresses")
    @Expose
    private List<Address> addresses = null;
    @SerializedName("contact_category")
    @Expose
    private ContactCategory contactCategory;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = -5328432171393434551L;

    /**
     * No args constructor for use in serialization
     *
     */
    public Contact() {
    }

    /**
     *
     * @param contactCategory
     * @param lastName
     * @param addresses
     * @param taxIdNumber
     * @param partyType
     * @param suffix
     * @param title
     * @param firstName
     * @param dob
     * @param middleName
     * @param company
     * @param id
     * @param department
     */
    public Contact(String partyType, String firstName, String lastName, String middleName, String suffix, String dob, String company, String department, String title, String taxIdNumber, List<Address> addresses, ContactCategory contactCategory, String id) {
        super();
        this.partyType = partyType;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.suffix = suffix;
        this.dob = dob;
        this.company = company;
        this.department = department;
        this.title = title;
        this.taxIdNumber = taxIdNumber;
        this.addresses = addresses;
        this.contactCategory = contactCategory;
        this.id = id;
    }

    public String getPartyType() {
        return partyType;
    }

    public void setPartyType(String partyType) {
        this.partyType = partyType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTaxIdNumber() {
        return taxIdNumber;
    }

    public void setTaxIdNumber(String taxIdNumber) {
        this.taxIdNumber = taxIdNumber;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public ContactCategory getContactCategory() {
        return contactCategory;
    }

    public void setContactCategory(ContactCategory contactCategory) {
        this.contactCategory = contactCategory;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("partyType", partyType).append("firstName", firstName).append("lastName", lastName).append("middleName", middleName).append("suffix", suffix).append("dob", dob).append("company", company).append("department", department).append("title", title).append("taxIdNumber", taxIdNumber).append("addresses", addresses).append("contactCategory", contactCategory).append("id", id).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(contactCategory).append(lastName).append(addresses).append(taxIdNumber).append(partyType).append(suffix).append(title).append(firstName).append(dob).append(middleName).append(company).append(id).append(department).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Contact) == false) {
            return false;
        }
        Contact rhs = ((Contact) other);
        return new EqualsBuilder().append(contactCategory, rhs.contactCategory).append(lastName, rhs.lastName).append(addresses, rhs.addresses).append(taxIdNumber, rhs.taxIdNumber).append(partyType, rhs.partyType).append(suffix, rhs.suffix).append(title, rhs.title).append(firstName, rhs.firstName).append(dob, rhs.dob).append(middleName, rhs.middleName).append(company, rhs.company).append(id, rhs.id).append(department, rhs.department).isEquals();
    }

}