package com.respec.billing.accounts.contracts;

public interface ConfigurationRepository<T> {

    T getConfigurationByTenantId(String tenantId);

}
