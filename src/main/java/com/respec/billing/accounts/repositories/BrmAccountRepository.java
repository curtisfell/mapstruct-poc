package com.respec.billing.accounts.repositories;

import com.respec.billing.accounts.models.brm.Account;
import com.respec.billing.accounts.contracts.AccountRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class BrmAccountRepository implements AccountRepository<Account>
{

    @Override
    public List<Account> listAccountsByTenantId(String tenantId) {

        return mockBrmAccounts(tenantId, 0);

    }

    @Override
    public Account getAccountById(String tenantId, int accountId) {

        List<Account> x = mockBrmAccounts(tenantId, accountId);
        Account y = x.get(1);
        //return mockBrmAccounts(tenantId, accountId).get(0);
        return y;

    }

    private List<Account> mockBrmAccounts(String tenantId, int accountId) {

      List<Account> accounts = new ArrayList<>();
      for(int i=1; i<5; i++) {
        String pid = "01234567" + String.valueOf(accountId);
        accounts.add(new Account(String.valueOf(accountId), String.format("%s-%s", tenantId, pid), "3", 1));
      }
      return accounts;

    }
}
