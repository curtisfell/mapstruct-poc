package com.respec.billing.common;


import java.util.EnumSet;

public class AccountConfiguration {

    String tenantId;
    BillingProvider provider;

    public enum BillingProvider {
        GTV ("GTV"),
        BRM ("BRM");
        private final String value;
        private BillingProvider(final String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    public EnumSet<BillingProvider> billingProviders = EnumSet.of(BillingProvider.GTV, BillingProvider.BRM);

    public AccountConfiguration(){}
    public AccountConfiguration(String tenantId){

        //TODO for demo purposes ONLY - anything but tenant id "1" is BRM
        this.tenantId = tenantId;
        if (tenantId.equalsIgnoreCase("1")) {
            this.provider = BillingProvider.GTV;
        } else {
            this.provider = BillingProvider.BRM;
        }

    }

    //TODO what else?

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public BillingProvider getProvider() {
        return provider;
    }

    public void setProvider(BillingProvider provider) {
        this.provider = provider;
    }

    public EnumSet<BillingProvider> getBillingProviders() {
        return billingProviders;
    }

    public void setBillingProviders(EnumSet<BillingProvider> billingProviders) {
        this.billingProviders = billingProviders;
    }
}
