package com.respec.billing.mappers;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.respec.billing.accounts.models.gtv.CpqDiscount;
import com.respec.billing.accounts.models.GenericDiscount;
import org.springframework.core.convert.converter.Converter;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GtvCpqDiscountMapper {

    @Mapping(source = "id", target = "id")
    @Mapping(source = "startDate", target = "startDate")
    @Mapping(source = "deactivationDate", target = "deactivationDate")
    @Mapping(source = "duration", target = "duration")
    @Mapping(source = "recurrence", target = "recurrence")
    @Mapping(source = "value", target = "value")
    @Mapping(source = "applicationType", target = "applicationType")
    @Mapping(source = "priceType", target = "priceType")
    @Mapping(source = "discountKey", target = "discountKey")
    GenericDiscount convert(GenericDiscount source);

    //@InheritInverseConfiguration
    //CpqDiscount targetToSource(GtvCpqDiscount target);

}
