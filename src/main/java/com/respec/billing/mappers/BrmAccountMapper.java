package com.respec.billing.mappers;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.respec.billing.accounts.models.brm.Account;
import com.respec.billing.accounts.models.GenericAccount;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BrmAccountMapper {
    @Mapping(source = "accountNbr", target = "accountNumber")
    @Mapping(source = "parentAccountNbr", target = "parentAccountId")
    @Mapping(source = "accountType", target = "type")
    @Mapping(source = "currencyType", target = "currency")
    GenericAccount convert(Account source);

    //@InheritInverseConfiguration
    //Account targetToSource(GenericAccount target);

}
