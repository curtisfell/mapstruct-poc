package com.respec.billing.mappers;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.respec.billing.accounts.models.gtv.BillCycle;
import com.respec.billing.accounts.models.GenericBillCycle;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GtvBillCycleMapper {

    @Mapping(source = "id", target = "id")
    @Mapping(source = "billCycleType", target = "billCycleType")
    @Mapping(source = "name", target = "name")
    GenericBillCycle convert(BillCycle source);

    //@InheritInverseConfiguration
    //BillCycle targetToSource(GtvBillCycle target);

}
