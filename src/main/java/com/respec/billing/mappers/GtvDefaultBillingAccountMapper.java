package com.respec.billing.mappers;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.respec.billing.accounts.models.gtv.DefaultBillingAccount;
import com.respec.billing.accounts.models.GenericDefaultAccount;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GtvDefaultBillingAccountMapper {

    @Mapping(source = "id", target = "id")
    @Mapping(source = "accountNum", target = "accountNum")
    @Mapping(source = "externalAccountNum", target = "externalAccountNum")
    @Mapping(source = "cpqDiscounts", target = "genericDiscounts")
    GenericDefaultAccount convert(DefaultBillingAccount source);

    //@InheritInverseConfiguration
    //DefaultBillingAccount targetToSource(GtvDefaultBillingAccount target);

}
