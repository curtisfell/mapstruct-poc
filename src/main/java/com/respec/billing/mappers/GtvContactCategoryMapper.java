package com.respec.billing.mappers;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.respec.billing.accounts.models.gtv.ContactCategory;
import com.respec.billing.accounts.models.GenericContactCategory;
import org.springframework.core.convert.converter.Converter;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GtvContactCategoryMapper {

    @Mapping(source = "id", target = "id")
    @Mapping(source = "name", target = "name")
    GenericContactCategory convert(ContactCategory source);

    //@InheritInverseConfiguration
    //ContactCategory targetToSource(GtvContactCategory target);

}
